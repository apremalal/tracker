$(function() {
	var settings = "";
	settings += "<div class=\"btn-group\">";
	settings += "<button class=\"btn btn-mini btn-default dropdown-toggle\"";
	settings += "data-toggle=\"dropdown\">";
	settings += "<i class=\"icon-cog\"><\/i>";
	settings += "<\/button>";
	settings += "<ul class=\"dropdown-menu\">";
	settings += "<li><a href=\"#\" id=\"deleteuser\">Delete User<\/a><\/li>";
	settings += "<li><a href=\"#\">Deactivate<\/a><\/li>";
	settings += "<li><a href=\"#\">Activate<\/a><\/li>";
	settings += "<li class=\"divider\"><\/li>";
	settings += "<li><a href=\"#\">Separated link<\/a><\/li>";
	settings += "<\/ul>";
	settings += "<\/div>";

	$.ajax({
		url : contextPath + '/admin/users/getlist',
		type : 'POST',
		success : function(result) {
			console.log('success');
			console.log(result);
			var jsonObj = jQuery.parseJSON(result);
			if (jsonObj.status == 'SUCCESS') {
				$.each(jsonObj.data, function(i, obj) {
					var tr = '<tr class=\"status-success\"'+'value='+obj.id +' id=row'+obj.id +'>'
							+ '<td class=\"userid\">' + obj.id + '</td>'
							+ '<td>' + obj.userName + '</td>' + '<td>'
							+ obj.firstName + '</td>' + '<td>' + obj.lastName
							+ '</td>' + '<td>' + obj.email + '</td>' + '<td>'
							+ obj.roles + '</td>' + '<td>' + settings + '</td>'
							+ '</tr>'
					$('#users_table tr:last').after(tr);
				});
			}
		},
		error : function(xhr, ajaxOptions, thrownError) {
			console.log('failed');
			console.log(xhr.status);
			console.log(thrownError);
		}
	});

});

$(document).on('click','#deleteuser',function(e) {
	var userId = $(this).closest('tr').attr('value');
	$.ajax({
		url : contextPath + '/admin/users/remove',
		type : 'POST',
		data : 'userId=' + userId,
		success : function(result) {
			console.log('success');
			console.log(result);
			var jsonObj = jQuery.parseJSON(result);
			if (jsonObj.status == 'SUCCESS') {
					$('#row'+userId).remove();
			}
		},
		error : function(xhr, ajaxOptions, thrownError) {
			console.log('failed');
			console.log(xhr.status);
			console.log(thrownError);
		}
	});
	}
);
