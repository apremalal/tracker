/**
 * Main javascript controller of the Mylinex tracker web app.
 */

$(function()
{
	$("#import_clients").click(function(e) {
		$("#import_modal").modal({
			keyboard : true,
			pause : "hover",
			show : true
		});
	});
	
	$("#fileupload").fileupload({
		dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo(document.body);
            });
        }
    });
});