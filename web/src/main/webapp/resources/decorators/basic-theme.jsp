<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<title>Tracker</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="Anuruddha Premalal">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/font-awesome.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/css/bootstrap-responsive.min.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/master.css" />"
	rel="stylesheet" type="text/css" />

<style type="text/css">
body,html {
	height: 100%;
}

.navbar {
	margin-bottom: 0;
}

.footer .container {
	width: auto;
}

.wrap>.container {
	width: 100%;
	margin-left: auto;
	margin-right: auto;
}

.sitemesh-body {
	padding-top: 41px;
	height: 100%
}
/* Wrapper for page content to push down footer */
#wrap {
	min-height: 100%;
	margin: 0 auto -250px;
	/* Negative indent footer by it's height */
}

.footer,.push {
	height: 250px;
}

/* MARKETING CONTENT
    -------------------------------------------------- */

/* Center align the text within the three columns below the carousel */
.marketing .span4 {
	
}

.marketing h2 {
	font-weight: normal;
}

.marketing .span4 p {
	margin-left: 10px;
	margin-right: 10px;
}

/*Fitting the body contetn to browser width*/
@media ( max-width :979px) {
	body {
		padding-left: 0;
		padding-right: 0;
	}
	.global-searchbar {
		position: absolute;
		top: 30%;
	}
	/*Fixing the navbar to fill the browser width*/
	.navbar-fixed-top {
		margin-right: 0;
		margin-left: 0;
	}
	.carousel-inner .welcome-message h1 {
		font-size: 20px;
	}
	.welcome-message {
		display: none;
	}
}

@media screen and (max-width:1000px) {
	.carousel-inner .welcome-message {
		position: absolute;
		z-index: 100;
		top: 10%;
	}
	.global-searchbar input {
		padding: 8px 10px 8px 10px;
		margin: auto;
		width: 70%;
	}
}

@media ( max-width :886px) {
	.global-searchbar .btn.btn-primary {
		padding: 8px 0px 8px 0px;
	}
	.global-searchbar .btn.btn-primary .btn-text {
		visibility: hidden;
	}
}

@media ( max-width :459px) {
	.global-searchbar .btn.btn-primary {
		padding: 8px 0px 8px 0px;
		visibility: hidden;
	}
	.global-searchbar .btn.btn-primary .btn-text {
		visibility: hidden;
	}
}
</style>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
   <script src="../assets/js/html5shiv.js"></script>
 <![endif]-->
<decorator:head></decorator:head>
</head>

<body>

	<!-- NAVBAR ================================================== -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container ">
				<button type="button" class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="brand" href="${pageContext.request.contextPath}/">Airtel</a>

				<div class="nav-collapse collapse ">
					<form class="navbar-search pull-right">
						<input type="text" class="search-query" placeholder="Search">
					</form>
					<ul class="nav pull-right">
						<li class="divider-vertical"></li>
						<li class="dropdown"><sec:authorize access="authenticated"
								var="authenticated" /> <c:choose>
								<c:when test="${authenticated}">
									<a href="#" class="dropdown-toggle" role="button"
										data-toggle="dropdown"> <sec:authentication
											property="name" /> <b class="caret"></b>
									</a>
									<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
										<c:url var="logoutUrl" value="/logout" />
										<li><a id="navLogoutLink" href="${logoutUrl}">Logout</a></li>
										<li><a id="navLogoutLink" href="${logoutUrl}">Settings</a></li>
									</ul>
								</c:when>
								<c:otherwise>
									<c:url var="loginUrl" value="/login" />
									<li><a id="navLoginLink" href="${loginUrl}">Login</a></li>
								</c:otherwise>
							</c:choose></li>
						<li class="divider-vertical"></li>
					</ul>

				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>

	<div class="sidebar-background">
		<div class="primary-sidebar-background"></div>
	</div>
	<div class="primary-sidebar">
		<ul class="nav nav-collapse collapse nav-collapse-primary">
			<li class="active"><span class="glow"></span> <a
				href="dashboard"> <i class="icon-edit icon-2x"></i> <span>DashBoard</span>
			</a></li>

			<li class="dark-nav "><span class="glow"></span> <a
				class="accordion-toggle collapsed " data-toggle="collapse"
				href="#KBVCfTNBEv"> <i class="icon-beaker icon-2x"></i> <span>
						Manager <i class="icon-caret-down"></i>
				</span>

			</a>

				<ul id="KBVCfTNBEv" class="collapsed">

					<li class=""><a href="../ui_lab/buttons.html"> <i
							class="icon-hand-up "></i> Notify
					</a></li>

					<li class=""><a href="../ui_lab/general.html"> <i
							class="icon-beaker"></i> Performance
					</a></li>

					<li class=""><a href="../ui_lab/icons.html"> <i
							class="icon-info-sign"></i> Info
					</a></li>

					<li class=""><a href="../ui_lab/grid.html"> <i
							class="icon-th-large"></i> Groups
					</a></li>

					<li class=""><a href="../ui_lab/tables.html"> <i
							class="icon-table"></i> Tables
					</a></li>

					<li class=""><a href="../ui_lab/widgets.html"> <i
							class="icon-plus-sign-alt"></i> Add
					</a></li>

				</ul></li>

			<li class=""><span class="glow"></span> <a href="#"> <i
					class="icon-edit icon-2x"></i> <span>Forms</span>
			</a></li>
			<li class=""><span class="glow"></span> <a href="#"> <i
					class="icon-bar-chart icon-2x"></i> <span>Charts</span>
			</a></li>
			<li class="dark-nav "><span class="glow"></span> <a
				class="accordion-toggle collapsed " data-toggle="collapse"
				href="#w7Kr76mSff"> <i class="icon-link icon-2x"></i> <span>
						Others <i class="icon-caret-down"></i>
				</span>

			</a>

				<ul id="w7Kr76mSff" class="collapse ">

					<li class=""><a href="#"> <i class="icon-magic"></i>
							Promotions
					</a></li>

					<li class=""><a href="admin/users"> <i class="icon-user"></i>
							User Management
					</a></li>

					<li class=""><a href="#"> <i class="icon-user"></i> Sign
							Up
					</a></li>

					<li class=""><a href="#"> <i class="icon-calendar"></i>
							Full Calendar
					</a></li>
				</ul></li>
		</ul>
	</div>

	<div class="sitemesh-body">
		<div class="main-content">
			<decorator:body />
		</div>
	</div>

	<!-- Modals -->
	<div id="import_modal" class="modal hide fade" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">×</button>
			<h3 id="myModalLabel">Modal header</h3>
		</div>
		<div class="modal-body">
			<input type="file" name="files[]" class="btn btn-primary" id="fileupload" multiple>Upload file</input>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</div>
	<!-- ---------------- javascript---------------------  -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery/jquery-1.8.0-min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/holder.js"></script>

	<script src="${pageContext.request.contextPath}/resources/js/file_upload/js/vendor/jquery.ui.widget.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/file_upload/js/jquery.iframe-transport.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/file_upload/js/jquery.fileupload.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
	<script type="text/javascript">
		var contextPath = "${pageContext.request.contextPath}";
		var baseUrl = "http://localhost:8080/tracke";
	</script>
</body>
</html>