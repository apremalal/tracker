<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport"
	content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">


<meta charset="utf-8">

<!-- Always force latest IE rendering engine or request Chrome Frame -->
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">

<!-- Use title if it's in the page YAML frontmatter -->
<title>Core Admin Theme</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />

<link href="<c:url value="/resources/css/login.css" />" rel="stylesheet"
	type="text/css" />

<!--[if lt IE 9]>
  <script src="../../javascripts/vendor/html5shiv.js" type="text/javascript"></script>
  <script src="../../javascripts/vendor/excanvas.js" type="text/javascript"></script>
  <![endif]-->
</head>

<body>
	<div class="container-fluid">
		<div class="row-fluid">

			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-dashboard"></i> Dashboard
					</h3>
					<h5>Airtel Tracker dashboard</h5>
				</div>

				<ul class="inline pull-right sparkline-box">

					<li class="sparkline-row">
						<h4 class="blue">
							<span>Orders</span> 847
						</h4>
						<div class="sparkline big" data-color="blue">
							<!--6,26,17,3,4,24,4,7,26,8,23,11-->
						</div>
					</li>

					<li class="sparkline-row">
						<h4 class="green">
							<span>Reviews</span> 223
						</h4>
						<div class="sparkline big" data-color="green">
							<!--26,22,18,28,15,9,19,29,23,9,21,25-->
						</div>
					</li>

					<li class="sparkline-row">
						<h4 class="red">
							<span>New visits</span> 7930
						</h4>
						<div class="sparkline big">
							<!--3,23,25,14,13,18,29,14,29,26,11,5-->
						</div>
					</li>

				</ul>
			</div>
		</div>
	</div>

	<div class="container-fluid padded">
		<div class="row-fluid">

			<!-- Breadcrumb line -->

			<div id="breadcrumbs">
				<div class="breadcrumb-button blue">
					<span class="breadcrumb-label"><i class="icon-home"></i>
						Home</span> <span class="breadcrumb-arrow"><span></span></span>
				</div>



				<div class="breadcrumb-button">
					<span class="breadcrumb-label"> <i class="icon-dashboard"></i>
						Dashboard
					</span> <span class="breadcrumb-arrow"><span></span></span>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid padded">
		<div class="row-fluid">

			<div class="span3">
				<!-- find me in partials/action_nav_normal -->

				<!--big normal buttons-->
				<div class="action-nav-normal">

					<div class="row-fluid">
						<div class="span6 action-nav-button">
							<a href="#" title="New Group"> <i class="icon-file-alt"></i>
								<span>New Group</span>
							</a> <span class="triangle-button red"><i class="icon-plus"></i></span>
						</div>

						<div class="span6 action-nav-button">
							<a href="#" title="Messages"> <i class="icon-comments-alt"></i>
								<span>Messages</span>
							</a> <span class="label label-black">14</span>
						</div>
					</div>

					<div class="row-fluid">
						<div class="span6 action-nav-button">
							<a href="#" id="import_clients" title="import-clients"> <i
								class="icon-arrow-down"></i> <span>Import Clients</span>
							</a>
						</div>

						<div class="span6 action-nav-button">
							<a href="#" title="Users"> <i class="icon-user"></i> <span>Users</span>
							</a> <span class="triangle-button green"><span class="inner">+3</span></span>
						</div>
					</div>

					<div class="row-fluid">
						<div class="span6 action-nav-button">
							<a href="#" title="All clients"> <i class="icon-group"></i> <span>All
									clients</span>
							</a>
						</div>

						<div class="span6 action-nav-button">
							<a href="#" title="Followers"> <i class="icon-twitter"></i> <span>Followers</span>
							</a> <span class="triangle-button blue"><span class="inner">+8</span></span>
						</div>
					</div>

				</div>
			</div>

			<div class="span9">
				<!-- find me in partials/big_chart -->

				<div class="box">
					<div class="box-header">
						<div class="title">Charts</div>
						<ul class="box-toolbar">
							<li class="toolbar-link"><a href="#" data-toggle="dropdown"><i
									class="icon-cog"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#"><i class="icon-plus-sign"></i> Add</a></li>
									<li><a href="#"><i class="icon-remove-sign"></i>
											Remove</a></li>
									<li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
								</ul></li>
						</ul>
					</div>

					<div class="box-content padded">
						<div class="row-fluid">
							<div class="span4 separate-sections" style="margin-top: 5px;">

								<div class="row-fluid">
									<div class="span12">
										<div class="dashboard-stats">
											<ul class="inline">
												<li class="glyph"><i class="icon-bolt icon-2x"></i></li>
												<li class="count">973,119</li>
											</ul>
											<div class="progress progress-blue">
												<div class="bar tip" title="80%" data-percent="80"></div>
											</div>
											<div class="stats-label">Total Messages</div>
										</div>
									</div>
								</div>

								<div class="row-fluid" style="margin-top: 30px;">
									<div class="span6">
										<div class="dashboard-stats small">
											<ul class="inline">
												<li class="glyph"><i class="icon-user"></i></li>
												<li class="count">8,800</li>
											</ul>
											<div class="progress progress-blue">
												<div class="bar tip" title="65%" data-percent="65"></div>
											</div>
											<div class="stats-label">New Visitors</div>
										</div>
									</div>

									<div class="span6">
										<div class="dashboard-stats small">
											<ul class="inline">
												<li class="glyph"><i class="icon-eye-open"></i></li>
												<li class="count">25.668</li>
											</ul>
											<div class="progress progress-blue">
												<div class="bar tip" title="80%" data-percent="80"></div>
											</div>
											<div class="stats-label">Page Views</div>
										</div>
									</div>
								</div>

							</div>
							<div class="span8">
								<div class="sine-chart" id="xchart-sine"></div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="row-fluid">
			<div class="span6">
				<div class="box">
					<div class="box-header">
						<div class="title">Full calendar</div>
					</div>

					<div class="box-content">
						<div id="calendar"></div>
					</div>
				</div>
			</div>

			<div class="span6">
				<div class="box">
					<div class="box-header">
						<span class="title">News with avatars (scrollable box)</span>
						<ul class="box-toolbar">
							<li><span class="label label-blue">178</span></li>
						</ul>
					</div>
					<div class="box-content scrollable"
						style="height: 552px; overflow-y: auto">
						<!-- find me in partials/news_with_icons -->

						<div class="box-section news with-icons">
							<div class="avatar blue">
								<i class="icon-ok icon-2x"></i>
							</div>
							<div class="news-time">
								<span>06</span> jan
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Twitter Bootstrap v3.0 is coming!</a>
								</div>
								<div class="news-text">With 2.2.2 out the door, our
									attention has shifted almost entirely to the next major update
									to the project ...</div>
							</div>
						</div>

						<div class="box-section news with-icons">
							<div class="avatar green">
								<i class="icon-lightbulb icon-2x"></i>
							</div>
							<div class="news-time">
								<span>11</span> feb
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Ruby on Rails 4.0</a>
								</div>
								<div class="news-text">Rails 4.0 is still unfinished, but
									it is shaping up to become a great release ...</div>
							</div>
						</div>

						<div class="box-section news with-icons">
							<div class="avatar purple">
								<i class="icon-mobile-phone icon-2x"></i>
							</div>
							<div class="news-time">
								<span>04</span> mar
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">All about SCSS</a>
								</div>
								<div class="news-text">Sass makes CSS fun again. Sass is
									an extension of CSS3, adding nested rules ...</div>
							</div>
						</div>


						<div class="box-section news with-icons">
							<div class="avatar cyan">
								<i class="icon-ok icon-2x"></i>
							</div>
							<div class="news-time">
								<span>22</span> dec
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Twitter Bootstrap v3.0 is coming!</a>
								</div>
								<div class="news-text">With 2.2.2 out the door, our
									attention has shifted almost entirely to the next major update
									to the project ...</div>
							</div>
						</div>


						<!-- find me in partials/news_with_icons -->

						<div class="box-section news with-icons">
							<div class="avatar blue">
								<i class="icon-ok icon-2x"></i>
							</div>
							<div class="news-time">
								<span>06</span> jan
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Twitter Bootstrap v3.0 is coming!</a>
								</div>
								<div class="news-text">With 2.2.2 out the door, our
									attention has shifted almost entirely to the next major update
									to the project ...</div>
							</div>
						</div>

						<div class="box-section news with-icons">
							<div class="avatar green">
								<i class="icon-lightbulb icon-2x"></i>
							</div>
							<div class="news-time">
								<span>11</span> feb
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Ruby on Rails 4.0</a>
								</div>
								<div class="news-text">Rails 4.0 is still unfinished, but
									it is shaping up to become a great release ...</div>
							</div>
						</div>

						<div class="box-section news with-icons">
							<div class="avatar purple">
								<i class="icon-mobile-phone icon-2x"></i>
							</div>
							<div class="news-time">
								<span>04</span> mar
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">All about SCSS</a>
								</div>
								<div class="news-text">Sass makes CSS fun again. Sass is
									an extension of CSS3, adding nested rules ...</div>
							</div>
						</div>


						<div class="box-section news with-icons">
							<div class="avatar cyan">
								<i class="icon-ok icon-2x"></i>
							</div>
							<div class="news-time">
								<span>22</span> dec
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Twitter Bootstrap v3.0 is coming!</a>
								</div>
								<div class="news-text">With 2.2.2 out the door, our
									attention has shifted almost entirely to the next major update
									to the project ...</div>
							</div>
						</div>


						<!-- find me in partials/news_with_icons -->

						<div class="box-section news with-icons">
							<div class="avatar blue">
								<i class="icon-ok icon-2x"></i>
							</div>
							<div class="news-time">
								<span>06</span> jan
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Twitter Bootstrap v3.0 is coming!</a>
								</div>
								<div class="news-text">With 2.2.2 out the door, our
									attention has shifted almost entirely to the next major update
									to the project ...</div>
							</div>
						</div>

						<div class="box-section news with-icons">
							<div class="avatar green">
								<i class="icon-lightbulb icon-2x"></i>
							</div>
							<div class="news-time">
								<span>11</span> feb
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Ruby on Rails 4.0</a>
								</div>
								<div class="news-text">Rails 4.0 is still unfinished, but
									it is shaping up to become a great release ...</div>
							</div>
						</div>

						<div class="box-section news with-icons">
							<div class="avatar purple">
								<i class="icon-mobile-phone icon-2x"></i>
							</div>
							<div class="news-time">
								<span>04</span> mar
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">All about SCSS</a>
								</div>
								<div class="news-text">Sass makes CSS fun again. Sass is
									an extension of CSS3, adding nested rules ...</div>
							</div>
						</div>


						<div class="box-section news with-icons">
							<div class="avatar cyan">
								<i class="icon-ok icon-2x"></i>
							</div>
							<div class="news-time">
								<span>22</span> dec
							</div>
							<div class="news-content">
								<div class="news-title">
									<a href="#">Twitter Bootstrap v3.0 is coming!</a>
								</div>
								<div class="news-text">With 2.2.2 out the door, our
									attention has shifted almost entirely to the next major update
									to the project ...</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
