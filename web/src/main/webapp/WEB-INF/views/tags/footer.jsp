<div class="global-footer">	     
	     	 <div class="row-fluid top-section"> 
	     	 	<div class="span8">
	     	 	<h2>FEATURED IN:</h2>
		     	 	<ul>
		     	 		<a class="yui3-u-1-2" href="/about"><img src="${pageContext.request.contextPath}/resources/icons/newyorktimes.png"></a>
						<a class="yui3-u-1-2" href="/jobs"><img src="${pageContext.request.contextPath}/resources/icons/business_insider.png"></a>
						<a class="yui3-u-1-2" href="/partner"><img src="${pageContext.request.contextPath}/resources/icons/forbes.png"></a>
						<a class="yui3-u-1-2" href="/terms"><img src="${pageContext.request.contextPath}/resources/icons/usa_today.png"></a>
		     	 	</ul>
	     	 	</div>
	     	 	<div class="span4 site-content">
	     	 		<div class="span8 nav-links">
	     	 		<div class=span6>
	     	 			<p>
	     	 				<a class="yui3-u-1-2" href="views/aboutus">About Us</a>
							<a class="yui3-u-1-2" href="views/jobs">Jobs</a>
							<a class="yui3-u-1-2" href="views/partner">Partner with Peek</a>							
     	 				</p>
	     	 		</div>
	     	 		<div class=span6>
	     	 			<p>
	     	 				<a class="yui3-u-1-2" href="/terms">Terms of Use</a>
							<a class="yui3-u-1-2" href="/privacy">Privacy Policy</a>
							<a class="yui3-u-1-2" href="/attribution">Image Attribution</a>	
	     	 			</p>
	     	 		</div>
	     	 			
	     	 		</div>
	     	 		<div class="span4 media-links">
	     	 			<h3>Find us on:</h3>
	     	 			<a href="http://www.facebook.com/adenue" target="_blank" class="icon-facebook"></a>
	     	 			<a href="http://www.twitter.com/adenue" target="_blank" class="icon-twitter"></a>
	     	 			<a href="http://www.pinterest.com/peek" target="_blank" class="icon-pinterest"></a>
	     	 			<a href="http://www.instagram.com/peek" target="_blank" class="icon-instagram"></a>
	     	 		</div>
	     	 	</div>
		     </div>
		     <div class="row-fluid bottom-section">
			     	<div class="span6" >
				     	<h3>CALIFORNIA:</h3>
				     	<ul>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in San Francisco</a></li>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in Los Angeles</a></li>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in San Diego</a></li>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in Wine Country</a></li>
				     	</ul>
			     	</div>
			     	<div class="span6" >
				     	<h3>SINGAPORE:</h3>
				     	<ul>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in San Francisco</a></li>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in Los Angeles</a></li>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in San Diego</a></li>
				     	 <li class="yui-4-1"><a href=""#>Things to do<br>in Wine Country</a></li>
				     	</ul>
			     	</div>
		     </div>
	      </div>