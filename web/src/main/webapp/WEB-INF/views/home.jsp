<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
</head>
<body>

	<div id="map-canvas1"></div>

	<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSA6mVTRzc1XLNiv_EH8QFyTxaZFCDeKY&sensor=true">
		
	</script>
	<script type="text/javascript">
		function initialize() {
			var mapOptions = {
				center : new google.maps.LatLng(-34.397, 150.644),
				zoom : 8,
				mapTypeId : google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document
					.getElementById("map-canvas1"), mapOptions);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
</body>
</html>
