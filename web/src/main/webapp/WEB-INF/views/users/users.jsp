<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery-1.8.0-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/users.js"></script>
</head>
<body>
	<div class="container-fluid padded">
		<div class="row-fluid">
			<!-- find me in partials/tasks_table -->

			<div class="box">
				<div class="box-header">
					<span class="title"><i class="icon-group"></i> Registered
						Users</span>
					<ul class="box-toolbar">
						<li><button class="btn btn-info" id="newuser">New User</button></li>
					</ul>
				</div>
				<div class="box-content">
					<table class="table table-normal" id="users_table">
						<thead>
							<tr>
								<td></td>
								<td>User Name</td>
								<td>First Name</td>
								<td>Last Name</td>
								<td>Email</td>
								<td>Role</td>
								<td style="width: 40px"></td>
							</tr>
						</thead>

						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</body>
</html>