<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<html>
<head>
<meta name="decorator" content="signup">
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/css/bootstrap-responsive.min.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/signup.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/master.css" />"
	rel="stylesheet" type="text/css" />
<title>Registration</title>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 40px;
	background-color: #f5f5f5;
}

.form-signin {
	max-width: 300px;
	padding: 19px 29px 29px;
	margin-left: auto;
}

.form-signin .form-signin-heading,.form-signin .checkbox {
	margin-bottom: 10px;
}

.form-signin input[type="text"],.form-signin input[type="password"] {
	font-size: 16px;
	height: auto;
	margin-bottom: 15px;
	padding: 7px 9px;
}
</style>

</head>
<body>
	<c:set var="pageTitle" value="Please Login" scope="request" />
	<c:url value="/process_login" var="loginUrl" />
	<div class="container">
		<form class="form-signin well offset7" action="${loginUrl}" method="post">
			<c:if test="${param.error != null}">
				<div class="alert alert-error">
					Failed to login.
					<c:if test="${SPRING_SECURITY_LAST_EXCEPTION != null}">
				Reason: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
					</c:if>
				</div>
			</c:if>
			<c:if test="${param.logout != null}">
				<div class="alert alert-success">You have been logged out.</div>
			</c:if>
			<h2 class="form-signin-heading">Please sign in</h2>
			<input type="text" id="username" class="input-block-level" name="username"  placeholder="Email address">
				 <input type="password"	id="password" name="password" class="input-block-level" placeholder="Password">
			<label class="checkbox"> <input type="checkbox"
				value="remember-me"> Remember me
			</label>
			<input class="btn btn-large btn-info btn-block" id="submit"	name="submit" type="submit" value="Submit">
		</form>
	</div>


	<!-- ---------------- javascript---------------------  -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-1.8.0-min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/holder.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>
</html>

