<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="decorator" content="login">
<link href="<c:url value="/resources/css/login.css" />" rel="stylesheet"
	type="text/css" />
	<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/css/bootstrap-responsive.min.css" />"
	rel="stylesheet" type="text/css" />
<title>Registration</title>

<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery-1.8.0-min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// SUCCESS AJAX CALL, replace "success: false," by:     success : function() { callSuccessFunction() },
		//	$("#loginform").validationEngine();

	});
</script>
</head>
<body>
	<c:set var="pageTitle" value="Please Login" scope="request" />
	<c:url value="/process_login" var="loginUrl" />
	<div class="container">
		<form class="form-signin well" action="${loginUrl}" method="post"
			id="loginform">
			<c:if test="${param.login_message != null}">
				<div class="alert alert-error">
					<c:if test="${SPRING_SECURITY_LAST_EXCEPTION != null}">
						<%-- Reason: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" /> --%>
					User Name Password does not match
					</c:if>
				</div>
			</c:if>
			<c:if test="${param.logout_message != null}">
				<div class="alert alert-success">You have been logged out.</div>
			</c:if>
			<h2 class="form-signin-heading">Please sign in</h2>
			<input type="text" id="username"
				class="input-block-level username-field" name="username"
				placeholder="Email address"> <input type="password"
				id="password" name="password"
				class="input-block-level password-field" placeholder="Password">
			<input class="btn btn-large btn-info btn-block" id="submit"
				name="submit" type="submit" value="Submit"> <label
				class="checkbox"> <br> <input type="checkbox"
				id="remember" name="_spring_security_remember_me" value="true">
				Remember me?
			</label>
		</form>
					<div class="apptitle">
						<img alt="" src="resources/img/title.png">
					</div>
	</div>

</body>
</html>


