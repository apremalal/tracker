package com.mylinex.tracker.webappcommunication.util;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class ResponseGenerator {

	private ObjectMapper objectMapper;

	/**
	 * generates output with default success state
	 * 
	 * @param data
	 * @return
	 */
	public Result generateOutput(Object data) {

		return new Result(Status.SUCCESS, data);

	}

	/**
	 * generates output with given state information with default message
	 * 
	 * @param data
	 * @param status
	 * @return
	 */
	public Result generateOutput(Object data, Status status) {
		return new Result(status, data);
	}

	/**
	 * generates output with given state information with custom message
	 * 
	 * @param data
	 * @param status
	 * @param message
	 * @return
	 */

	public Result generateOutput(Object data, Status status, String message) {

		return new Result(status, data, message);
	}

	/**
	 * generates JSON String with default success state
	 * 
	 * @param data
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public String generateJSON(Object data) {
		try {
			return objectMapper.writeValueAsString(new Result(Status.SUCCESS,
					data));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * generates JSON String with given state information with default message
	 * 
	 * @param data
	 * @param status
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public String generateJSON(Object data, Status status) {

		try {
			return objectMapper.writeValueAsString(new Result(status, data));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * generates JSON String with given state information with custome message
	 * 
	 * @param data
	 * @param status
	 * @param message
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */

	public String generateJSON(Object data, Status status, String message) {
		try {
			return objectMapper.writeValueAsString(new Result(status, data,
					message));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
}
