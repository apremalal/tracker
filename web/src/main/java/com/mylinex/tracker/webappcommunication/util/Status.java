package com.mylinex.tracker.webappcommunication.util;

public enum Status {

	ERROR(500, "Error Occured while performing operatrion"), SUCCESS(100,
			"Operation compleated sucessfully"), AUTHENTICATION_FAILED(200,
			"You are not logged in, Please login to continue"), EMPTY_RESULT(
			300, "No results found"), SESSION_TIMEOUT(400,
			"Session has timeout. Please login again to continue");

	private int value;
	private String message;

	private Status(int value, String message) {
		this.value = value;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public int getValue() {
		return value;
	}
}
