package com.mylinex.tracker.webappcommunication.util;

public class Result {
	
	private Status status;
	private String message;
	private Object data;

	/**
	 * create packet with given default message for given status and data
	 * 
	 * @param status
	 * @param data
	 */

	public Result(Status status, Object data) {
		setData(data);
		setStatus(status);
		setMessage(status.getMessage());
	}

	/**
	 * create packet with given status ,data and custom message
	 * 
	 * @param status
	 * @param data
	 * @param message
	 */
	public Result(Status status, Object data, String message) {
		setData(data);
		setStatus(status);
		setMessage(message);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
