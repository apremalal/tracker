package com.mylinex.tracker.webapp.dao;

import java.util.List;

import com.mylinex.tracker.webapp.domain.Group;

public interface GroupDAO {
	public int createGroup(Group group);

	public boolean removeGroup(Integer id);

	public void updateGroup(Group group);

	public List<Group> findGroupByName(String name);
}
