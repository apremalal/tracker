package com.mylinex.tracker.webapp.serviceimpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylinex.tracker.webapp.dao.UserDAO;
import com.mylinex.tracker.webapp.domain.User;
import com.mylinex.tracker.webapp.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserDAO userDAO;

	@Override
	@Transactional
	public int createUser(User user) {
		return userDAO.createUser(user);
	}

	@Override
	@Transactional
	public boolean removeUser(Integer id) {
		return userDAO.removeUser(id);
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		userDAO.updateUser(user);
	}

	@Override
	@Transactional
	public void changePassWord(String oldPw, String newPw) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional
	public User getUserbyUserName(String userName) {
		return userDAO.getUserbyUserName(userName);
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
