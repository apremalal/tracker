package com.mylinex.tracker.webapp.utils;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.web.context.support.XmlWebApplicationContext;

public class CustomXmlWebApplicationContext extends XmlWebApplicationContext {
	
	//TODO inject the field value from applicaiotn.properties file
	private String applicationEnviornment;
	
	protected void initBeanDefinitionReader(
			XmlBeanDefinitionReader beanDefinitionReader) {
		super.initBeanDefinitionReader(beanDefinitionReader);
		if (true) {			
			beanDefinitionReader
					.setValidationMode(XmlBeanDefinitionReader.VALIDATION_NONE);
		}
	}
}