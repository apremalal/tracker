package com.mylinex.tracker.webapp.views;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ViewsController {

	@RequestMapping(value = "/aboutus", method = RequestMethod.GET)
	public String about(Model model) {
		return "tags/aboutus";
	}

	@RequestMapping(value = "/partner", method = RequestMethod.GET)
	public String partner(Model model) {
		return "tags/aboutus";
	}

	@RequestMapping(value = "/policy", method = RequestMethod.GET)
	public String privacy(Model model) {
		return "tags/policy";
	}
	

	@RequestMapping(value = "/jobs", method = RequestMethod.GET)
	public String jobs(Model model) {
		return "tags/jobs";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(HttpServletRequest req, HttpServletResponse res,
			Model model) {
		return "security/signup";
	}
	
	@RequestMapping(value = "/admin/users", method = RequestMethod.GET)
	public String users(HttpServletRequest req, HttpServletResponse res,
			Model model) {
		return "users/users";
	}
}
