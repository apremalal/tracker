package com.mylinex.tracker.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SecurityController {

//	@Autowired
//	private UserService userService;
//
//	@Autowired
//	private PasswordEncoder passwordEncoder;
//
//	@Autowired
//	private SpringSecurityUserContext springSecurityUserContext;
//
//	@RequestMapping(consumes="application/json",value = "/signup/new", method = RequestMethod.POST)
//	public String signUp(@RequestBody User user,Model model, RedirectAttributes redirectAttributes) {
//		String encodedPassword = passwordEncoder.encode(user.getPassword());
//		user.setPassword(encodedPassword);
//		Role role = new Role();
//		role.setName("ROLE_USER");
//		user.getRoles().add(role);
//		userService.createUser(user);
//		springSecurityUserContext.setCurrentUser(user);
//		redirectAttributes.addFlashAttribute("message", "Success");
//		return "redirect:/";
//	}

	@RequestMapping("/login")
	public String login(Model model, @RequestParam(required=false) String message) {
		model.addAttribute("message", message);
		return "security/login";
	}
	
	@RequestMapping(value = "/denied")
	public String denied() {
		return "security/denied";
	}

	@RequestMapping(value = "/login/failure")
	public String loginFailure() {
		String message = "Login Failure!";
		return "redirect:/login?message=" + message;
	}

	@RequestMapping(value = "/logout/success")
	public String logoutSuccess() {
		String message = "Logout Success!";
		return "redirect:/login?message=" + message;
	}

}
