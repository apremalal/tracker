package com.mylinex.tracker.webapp.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTS")
public class Client {
	@Id
	@Column(name = "CLIENT_ID")
	@GeneratedValue
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "mobile_number")
	private String mobile_number;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "groups", joinColumns = { @JoinColumn(name = "CLIENT_ID") }, inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
	private Set<Group> groups = new HashSet<Group>();

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
