package com.mylinex.tracker.webapp.service;

import com.mylinex.tracker.webapp.domain.Role;

public interface RoleService {
	public void createRole(Role role);

	public void removeRole(Role role);

	public Role getRoleByName(String name);
}
