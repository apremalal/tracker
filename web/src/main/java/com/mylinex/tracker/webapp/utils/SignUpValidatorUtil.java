package com.mylinex.tracker.webapp.utils;

import com.mylinex.tracker.webapp.domain.User;
import com.mylinex.tracker.webapp.service.UserService;

public class SignUpValidatorUtil {

	private UserService userService;

	public boolean isUserNameExists(String userName) {
		User user = userService.getUserbyUserName(userName);
		if (user != null)
			return true;
		else
			return false;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
