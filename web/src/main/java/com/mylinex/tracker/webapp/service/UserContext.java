package com.mylinex.tracker.webapp.service;

import com.mylinex.tracker.webapp.domain.User;

public interface UserContext {
	User getCurrentuser();

	void setCurrentUser(User user);
}
