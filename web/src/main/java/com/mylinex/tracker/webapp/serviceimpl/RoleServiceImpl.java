package com.mylinex.tracker.webapp.serviceimpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylinex.tracker.webapp.dao.RoleDAO;
import com.mylinex.tracker.webapp.domain.Role;
import com.mylinex.tracker.webapp.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	private RoleDAO roleDAO;

	@Override
	@Transactional
	public void createRole(Role role) {
		roleDAO.createRole(role);
	}

	@Override
	@Transactional
	public void removeRole(Role role) {
		roleDAO.removeRole(role);
	}

	@Override
	@Transactional
	public Role getRoleByName(String name) {
		return roleDAO.getRoleByName(name);
	}

	public RoleDAO getRoleDAO() {
		return roleDAO;
	}

	public void setRoleDAO(RoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}

}
