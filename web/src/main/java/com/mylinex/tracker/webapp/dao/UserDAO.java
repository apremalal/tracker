package com.mylinex.tracker.webapp.dao;

import com.mylinex.tracker.webapp.domain.User;

public interface UserDAO {
	public int createUser(User user);

	public boolean removeUser(Integer ide);

	public void updateUser(User user);

	public User getUserbyUserName(String userName);
}
