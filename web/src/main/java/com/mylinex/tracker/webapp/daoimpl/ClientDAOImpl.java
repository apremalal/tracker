package com.mylinex.tracker.webapp.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.mylinex.tracker.webapp.dao.ClientDAO;
import com.mylinex.tracker.webapp.domain.Client;

@Repository
public class ClientDAOImpl implements ClientDAO {

	private SessionFactory sessionFactory;

	@Override
	public int createClient(Client client) {
		sessionFactory.getCurrentSession().save(client);
		sessionFactory.getCurrentSession().flush();
		return client.getId();
	}

	@Override
	public boolean removeClient(Integer id) {
		try {
			Client client = (Client) sessionFactory.getCurrentSession().load(
					Client.class, id);
			if (client != null) {
				sessionFactory.getCurrentSession().delete(client);
				sessionFactory.getCurrentSession().flush();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void updateClient(Client client) {
		sessionFactory.getCurrentSession().update(client);
		sessionFactory.getCurrentSession().flush();
	}

	@Override
	public List<Client> findClientByMobileNum(String number) {
		String hql = "from Filerecord  WHERE mobile_number=" + "\'" + number
				+ "\'";
		ArrayList<Client> clientList = (ArrayList<Client>) sessionFactory
				.getCurrentSession().createQuery(hql).list();
		if (clientList.size() > 0)
			return clientList;
		else
			return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
