package com.mylinex.tracker.webapp.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.mylinex.tracker.webapp.dao.GroupDAO;
import com.mylinex.tracker.webapp.domain.Group;

@Repository
public class GroupDAOImpl implements GroupDAO {

	private SessionFactory sessionFactory;

	@Override
	public int createGroup(Group group) {
		sessionFactory.getCurrentSession().save(group);
		sessionFactory.getCurrentSession().flush();
		return group.getId();
	}

	@Override
	public boolean removeGroup(Integer id) {
		try {
			Group group = (Group) sessionFactory.getCurrentSession().load(
					Group.class, id);
			if (group != null) {
				sessionFactory.getCurrentSession().delete(group);
				sessionFactory.getCurrentSession().flush();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void updateGroup(Group group) {
		sessionFactory.getCurrentSession().update(group);
		sessionFactory.getCurrentSession().flush();
	}

	@Override
	public List<Group> findGroupByName(String name) {
		String hql = "from Group  WHERE name=" + "\'" + name + "\'";
		ArrayList<Group> groupList = (ArrayList<Group>) sessionFactory
				.getCurrentSession().createQuery(hql).list();
		if (groupList.size() > 0)
			return groupList;
		else
			return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
