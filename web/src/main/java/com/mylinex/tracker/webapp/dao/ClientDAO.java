package com.mylinex.tracker.webapp.dao;

import java.util.List;

import com.mylinex.tracker.webapp.domain.Client;

public interface ClientDAO {
	public int createClient(Client client);

	public boolean removeClient(Integer id);

	public void updateClient(Client client);

	public List<Client> findClientByMobileNum(String number);
}
