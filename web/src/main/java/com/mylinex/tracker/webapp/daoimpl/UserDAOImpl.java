package com.mylinex.tracker.webapp.daoimpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.mylinex.tracker.webapp.dao.UserDAO;
import com.mylinex.tracker.webapp.domain.User;

@Repository
public class UserDAOImpl implements UserDAO {

	private SessionFactory sessionFactory;

	@Override
	public int createUser(User user) {
		sessionFactory.getCurrentSession().save(user);
		sessionFactory.getCurrentSession().flush();
		return user.getId();
	}

	@Override
	public boolean removeUser(Integer id) {
		try {
			User user = (User) sessionFactory.getCurrentSession().load(
					User.class, id);
			if (user != null) {
				sessionFactory.getCurrentSession().delete(user);
				sessionFactory.getCurrentSession().flush();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void updateUser(User user) {
		sessionFactory.getCurrentSession().update(user);
		sessionFactory.getCurrentSession().flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public User getUserbyUserName(String userName) {
		String hql = "from User  WHERE userName=" + "\'" + userName + "\'";
		ArrayList<User> userList = (ArrayList<User>) sessionFactory
				.getCurrentSession().createQuery(hql).list();
		if (userList.size() > 0)
			return userList.get(0);
		else
			return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
