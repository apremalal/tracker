package com.mylinex.tracker.webapp.service;

import java.util.List;

import com.mylinex.tracker.webapp.domain.Message;

public interface MessageService {
	public int createMessage(Message message);

	public boolean removeMessge(Integer id);

	public void updateMessage(Message message);

	public List<Message> findMessageByText(String text);
}
