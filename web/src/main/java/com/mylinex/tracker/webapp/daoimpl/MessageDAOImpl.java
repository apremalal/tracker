package com.mylinex.tracker.webapp.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.mylinex.tracker.webapp.dao.MessageDAO;
import com.mylinex.tracker.webapp.domain.Message;

@Repository
public class MessageDAOImpl implements MessageDAO {

	private SessionFactory sessionFactory;

	@Override
	public int createMessage(Message message) {
		sessionFactory.getCurrentSession().save(message);
		sessionFactory.getCurrentSession().flush();
		return message.getId();
	}

	@Override
	public boolean removeMessge(Integer id) {
		try {
			Message message = (Message) sessionFactory.getCurrentSession()
					.load(Message.class, id);
			if (message != null) {
				sessionFactory.getCurrentSession().delete(message);
				sessionFactory.getCurrentSession().flush();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void updateMessage(Message message) {
		sessionFactory.getCurrentSession().update(message);
		sessionFactory.getCurrentSession().flush();
	}

	@Override
	public List<Message> findMessageByText(String text) {
		String hql = "from Message  WHERE message=" + "\'" + text + "\'";
		ArrayList<Message> messageList = (ArrayList<Message>) sessionFactory
				.getCurrentSession().createQuery(hql).list();
		if (messageList.size() > 0)
			return messageList;
		else
			return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
