package com.mylinex.tracker.webapp.serviceimpl;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylinex.tracker.webapp.domain.User;
import com.mylinex.tracker.webapp.service.UserService;
import com.mylinex.tracker.webapp.utils.AppUserAuthorityUtils;

@Service
@Transactional(readOnly = true)
public class CustomUserDetailService implements UserDetailsService {

	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		try {
			User user = userService.getUserbyUserName(userName);
			if (user != null) {
				return new AppUserDetails(user);
			} else {
				throw new UsernameNotFoundException("cannot find the user name");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private final class AppUserDetails extends User implements UserDetails {

		private static final long serialVersionUID = 14L;

		AppUserDetails(User user) {
			setId(user.getId());
			setUserName(user.getUserName());
			setEmail(user.getEmail());
			setFirstName(user.getFirstName());
			setLastName(user.getLastName());
			setPassword(user.getPassword());
			setRoles(user.getRoles());
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return AppUserAuthorityUtils.getAuthorities(getRoles());
		}

		@Override
		public String getUsername() {
			return getUserName();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
