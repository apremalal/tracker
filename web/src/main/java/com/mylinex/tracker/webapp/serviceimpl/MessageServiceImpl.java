package com.mylinex.tracker.webapp.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylinex.tracker.webapp.dao.MessageDAO;
import com.mylinex.tracker.webapp.domain.Message;
import com.mylinex.tracker.webapp.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {

	private MessageDAO messageDAO;

	@Override
	@Transactional
	public int createMessage(Message message) {
		return messageDAO.createMessage(message);
	}

	@Override
	@Transactional
	public boolean removeMessge(Integer id) {
		return messageDAO.removeMessge(id);
	}

	@Override
	@Transactional
	public void updateMessage(Message message) {
		messageDAO.updateMessage(message);
	}

	@Override
	@Transactional
	public List<Message> findMessageByText(String text) {
		return messageDAO.findMessageByText(text);
	}

	public MessageDAO getMessageDAO() {
		return messageDAO;
	}

	public void setMessageDAO(MessageDAO messageDAO) {
		this.messageDAO = messageDAO;
	}

}
