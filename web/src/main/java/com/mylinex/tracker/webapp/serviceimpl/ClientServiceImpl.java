package com.mylinex.tracker.webapp.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylinex.tracker.webapp.dao.ClientDAO;
import com.mylinex.tracker.webapp.domain.Client;
import com.mylinex.tracker.webapp.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

	private ClientDAO clientDAO;

	@Override
	@Transactional
	public int createClient(Client client) {
		return clientDAO.createClient(client);
	}

	@Override
	@Transactional
	public boolean removeClient(Integer id) {
		return clientDAO.removeClient(id);
	}

	@Override
	@Transactional
	public void updateClient(Client client) {
		clientDAO.updateClient(client);
	}

	@Override
	@Transactional
	public List<Client> findClientByMobileNum(String number) {
		return clientDAO.findClientByMobileNum(number);
	}

	public ClientDAO getClientDAO() {
		return clientDAO;
	}

	public void setClientDAO(ClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}

}
