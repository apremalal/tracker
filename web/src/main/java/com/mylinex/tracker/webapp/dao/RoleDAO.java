package com.mylinex.tracker.webapp.dao;

import com.mylinex.tracker.webapp.domain.Role;

public interface RoleDAO {
	public void createRole(Role role);

	public void removeRole(Role role);

	public void updateRole(Role role);
	
	public Role getRoleByName(String name);
}
