package com.mylinex.tracker.webapp.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylinex.tracker.webapp.dao.GroupDAO;
import com.mylinex.tracker.webapp.domain.Group;
import com.mylinex.tracker.webapp.service.GroupService;

@Service
public class GroupServiceImpl implements GroupService {

	private GroupDAO groupDAO;

	@Override
	@Transactional
	public int createGroup(Group group) {
		return groupDAO.createGroup(group);
	}

	@Override
	@Transactional
	public boolean removeGroup(Integer id) {
		return groupDAO.removeGroup(id);
	}

	@Override
	@Transactional
	public void updateGroup(Group group) {
		groupDAO.updateGroup(group);
	}

	@Override
	@Transactional
	public List<Group> findGroupByName(String name) {
		return groupDAO.findGroupByName(name);
	}

	public GroupDAO getGroupDAO() {
		return groupDAO;
	}

	public void setGroupDAO(GroupDAO groupDAO) {
		this.groupDAO = groupDAO;
	}

}
