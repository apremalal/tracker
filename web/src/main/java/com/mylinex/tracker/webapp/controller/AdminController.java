package com.mylinex.tracker.webapp.controller;

import java.util.Locale;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylinex.tracker.webapp.service.UserService;
import com.mylinex.tracker.webappcommunication.util.ResponseGenerator;
import com.mylinex.tracker.webappcommunication.util.Status;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	UserService userService;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	ResponseGenerator responseGenerator;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String dashboard(Locale locale, Model model) {
		return "users/users";
	}

	@RequestMapping(value = "/users/remove", method = RequestMethod.POST)
	public @ResponseBody
	String removeUser(@RequestParam(value = "userId", required = true) int id,
			Locale locale, Model model) {
		if (userService.removeUser(id))
			return responseGenerator.generateJSON(null, Status.SUCCESS);
		else
			return responseGenerator.generateJSON(null, Status.ERROR,
					"No users found");
	}

	@RequestMapping(value = "/users/create", method = RequestMethod.POST)
	public String createUser(Locale locale, Model model) {
		return "users/users";
	}

	@RequestMapping(value = "/users/update", method = RequestMethod.POST)
	public String updateUser(Locale locale, Model model) {
		return "users/users";
	}
}
