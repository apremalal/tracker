package com.mylinex.tracker.webapp.service;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.mylinex.tracker.webapp.domain.User;
import com.mylinex.tracker.webapp.utils.AppUserAuthorityUtils;

public class SpringSecurityUserContext implements UserContext {
	@Override
	public User getCurrentuser() {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		if (authentication == null) {
			return null;
		}
		return (User) authentication.getPrincipal();
	}

	@Override
	public void setCurrentUser(User user) {
		Collection<? extends GrantedAuthority> authorities = AppUserAuthorityUtils.getAuthorities(user
				.getRoles());
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				user.getUserName(), user.getPassword(), authorities);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
}
