package com.mylinex.tracker.webapp.service;

import com.mylinex.tracker.webapp.domain.User;

public interface UserService {
	public int createUser(User user);

	public boolean removeUser(Integer id);

	public void updateUser(User user);

	public void changePassWord(String oldPw, String newPw);

	public User getUserbyUserName(String userName);
}
