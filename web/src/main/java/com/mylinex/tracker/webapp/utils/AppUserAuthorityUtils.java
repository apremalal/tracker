package com.mylinex.tracker.webapp.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.mylinex.tracker.webapp.domain.Role;


public class AppUserAuthorityUtils {
	public static List<String> getRolesAsList(Set<Role> roles) {
		List<String> rolesAsList = new ArrayList<String>();
		for (Role role : roles) {
			rolesAsList.add(role.getName());
		}
		return rolesAsList;
	}

	public static List<GrantedAuthority> getGrantedAuthorities(
			List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	public static Collection<? extends GrantedAuthority> getAuthorities(
			Set<Role> roles) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRolesAsList(roles));
		return authList;
	}
}
