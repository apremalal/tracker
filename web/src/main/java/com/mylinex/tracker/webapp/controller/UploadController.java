package com.mylinex.tracker.webapp.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mylinex.tracker.webapp.domain.Client;
import com.mylinex.tracker.webapp.domain.FileRecord;
import com.mylinex.tracker.webapp.service.ClientService;
import com.mylinex.tracker.webapp.service.FileRecordsService;
import com.mylinex.tracker.webapp.service.SpringSecurityUserContext;

@Controller
public class UploadController {
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Value("${upload.file.path}")
	private String uploadFilePath;

	@Autowired
	private SpringSecurityUserContext springSecurityUserContext;

	@Autowired
	private FileRecordsService fileRecordsService;

	@Autowired
	private ClientService clientService;

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String home(HttpServletRequest req, HttpServletResponse res,
			Model model) {
		String storedFileName = null;
		try {			
			String originalFileName = null;
			String fileExtention = null;
			String userName = springSecurityUserContext.getCurrentuser()
					.getUserName();
			long fileSize = 0;

			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			Date date = new Date();
			Random randomGen = new Random();
			String uniqueid = dateFormat.format(date) + randomGen.nextInt(100);
			boolean fileSavedOK = false;
			String uniqueFileName = null;

			try {
				DiskFileItemFactory factory = new DiskFileItemFactory();
				factory.setSizeThreshold(4096);

				ServletFileUpload upload = new ServletFileUpload(factory);
				upload.setSizeMax(1000000);

				List<FileItem> fileItems = upload.parseRequest(req);
				FileItem fi = (FileItem) fileItems.get(0);
				fileSize = fi.getSize();
				originalFileName = fi.getName();

				fileExtention = originalFileName.substring(originalFileName
						.lastIndexOf("."));

				storedFileName = userName + uniqueid + fileExtention;
				uniqueFileName = userName + uniqueid;

				fi.write(new File(uploadFilePath + storedFileName));
				fileSavedOK = true;

			} catch (Exception e) {
				e.printStackTrace();
				fileSavedOK = false;
				model.addAttribute("msg",
						"Error saving Image to the Server Storage");
			}
			if (fileSavedOK) {
				FileRecord fileRecord = new FileRecord();
				fileRecord.setOriginalFileName(originalFileName);
				fileRecord.setStoredFileName(storedFileName);
				fileRecord.setFileExtention(fileExtention);
				fileRecord.setUploadedDate(date);
				fileRecord.setUserName(userName);
				fileRecordsService.createFileRecord(fileRecord);
			}
		} catch (Exception e) {
		}
		model.addAttribute("storedFileName", storedFileName);
		model.addAttribute("success", true);
		return "file upload success";

	}

	@RequestMapping(value = "/import", method = RequestMethod.GET)
	private String readImportedFile( @RequestParam(value="storedFileName",required=true)String storedFileName, @RequestParam(value="ignoreErrors",required=false) boolean ignoreErrors) {
		String csvFile = uploadFilePath + storedFileName;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] clientDetails = line.split(cvsSplitBy);
				if (clientDetails.length != 2)
					return "error in the contact list";
				else {
					Client client = new Client();
					client.setName(clientDetails[0]);
					client.setMobile_number(clientDetails[1]);
					clientService.createClient(client);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	private String validateClient(String client[]) {
		return null;
	}
}