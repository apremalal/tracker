package com.mylinex.tracker.webapp.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.mylinex.tracker.webapp.dao.FileRecordDAO;
import com.mylinex.tracker.webapp.domain.FileRecord;

@Repository
public class FileRecordDAOImpl implements FileRecordDAO {

	private SessionFactory sessionFactory;

	@Override
	public int createFileRecord(FileRecord fileRecord) {
		sessionFactory.getCurrentSession().save(fileRecord);
		sessionFactory.getCurrentSession().flush();
		return fileRecord.getId();
	}

	@Override
	public boolean removeFileRecord(Integer id) {
		try {
			FileRecord fileRecord = (FileRecord) sessionFactory
					.getCurrentSession().load(FileRecord.class, id);
			if (fileRecord != null) {
				sessionFactory.getCurrentSession().delete(fileRecord);
				sessionFactory.getCurrentSession().flush();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void updateFileRecord(FileRecord fileRecord) {
		sessionFactory.getCurrentSession().update(fileRecord);
		sessionFactory.getCurrentSession().flush();
	}

	@Override
	public List<FileRecord> getFileRecords(String userName) {
		String hql = "from Filerecord  WHERE userName=" + "\'" + userName
				+ "\'";
		ArrayList<FileRecord> fileRecordList = (ArrayList<FileRecord>) sessionFactory
				.getCurrentSession().createQuery(hql).list();
		if (fileRecordList.size() > 0)
			return fileRecordList;
		else
			return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
