package com.mylinex.tracker.webapp.service;

import java.util.List;

import com.mylinex.tracker.webapp.domain.FileRecord;

public interface FileRecordsService {
	public int createFileRecord(FileRecord fileRecord);

	public boolean removeFileRecord(Integer id);

	public void updateFileRecord(FileRecord fileRecord);

	public List<FileRecord> getFileRecords(String userName);
}
