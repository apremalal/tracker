package com.mylinex.tracker.webapp.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylinex.tracker.webapp.dao.FileRecordDAO;
import com.mylinex.tracker.webapp.domain.FileRecord;
import com.mylinex.tracker.webapp.service.FileRecordsService;

@Service
public class FileRecordServiceImpl implements FileRecordsService {

	private FileRecordDAO fileRecordDAO;

	@Override
	@Transactional
	public int createFileRecord(FileRecord fileRecord) {
		return fileRecordDAO.createFileRecord(fileRecord);
	}

	@Override
	@Transactional
	public boolean removeFileRecord(Integer id) {
		return fileRecordDAO.removeFileRecord(id);
	}

	@Override
	@Transactional
	public void updateFileRecord(FileRecord fileRecord) {
		fileRecordDAO.updateFileRecord(fileRecord);
	}

	@Override
	@Transactional
	public List<FileRecord> getFileRecords(String userName) {
		return fileRecordDAO.getFileRecords(userName);
	}

	public FileRecordDAO getFileRecordDAO() {
		return fileRecordDAO;
	}

	public void setFileRecordDAO(FileRecordDAO fileRecordDAO) {
		this.fileRecordDAO = fileRecordDAO;
	}

}
