package com.mylinex.tracker.webapp.daoimpl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.mylinex.tracker.webapp.dao.RoleDAO;
import com.mylinex.tracker.webapp.domain.Role;


@Repository
public class RoleDAOImpl implements RoleDAO {

	private SessionFactory sessionFactory;

	@Override
	public void createRole(Role role) {
		sessionFactory.getCurrentSession().save(role);
	}

	@Override
	public void removeRole(Role role) {
		sessionFactory.getCurrentSession().delete(role);
	}

	@Override
	public void updateRole(Role role) {
		sessionFactory.getCurrentSession().update(role);
	}

	@Override
	public Role getRoleByName(String name) {
		String hql = "from Role  WHERE name=" + "\'" + name + "\'";
		return (Role) sessionFactory.getCurrentSession().createQuery(hql)
				.list().get(0);
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
